<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\CustomerUser;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $faker;
    private $manager;
    private $hasheur;

    public function __construct(UserPasswordHasherInterface $hasheur)
    {
        $faker = Factory::create('fr_FR');
        $faker->addProvider(new \Liior\Faker\Prices($faker));
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));
        $this->hasheur = $hasheur;

        $this->faker = $faker;
    }
    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;

        $this->loadProduct();
        $this->loadCustomer();
        $this->loadUser();

        $this->manager->flush();
    }

    private function loadProduct()
    {
        for($loopProduct=0; $loopProduct < 100; $loopProduct++) {
            $product = new Product();
            $product->setName($this->faker->productName)
                    ->setDescription($this->faker->sentence())
                    ->setPrice($this->faker->price(10000,100000));
            $this->manager->persist($product);
        }   
    }

    private function loadCustomer()
    {
        for($loopCustomer=0; $loopCustomer < 100; $loopCustomer++) {
            $customer = new Customer();
            $customer->setName($this->faker->company())
                    ->setDescription($this->faker->text(200))
                    ->setEmail($this->faker->safeEmail());
            for($loopUser=0; $loopUser < mt_rand(50,100); $loopUser++) {
                $user = $this->loadUserCustomer();
                $customer->addCustomerUser($user);
            }
            $this->manager->persist($customer);
        }
    }

    private function loadUserCustomer()
    {
        $userCustomer = new CustomerUser();
        $userCustomer->setFirstName($this->faker->firstName())
            ->setLastName($this->faker->lastName())
            ->setEmail($this->faker->email())
            ->setUsername($this->faker->userName())
            ->setCreatedAt(new \Datetime());
        
        $this->manager->persist($userCustomer);
        return $userCustomer;
    }

    private function loadUser()
    {
        $user = new User();
        $user->setEmail("root@root.fr")
            ->setUsername("root")
            ->setPassword($this->hasheur->hashPassword($user,"root"))
            ->setRoles(["ROLE_USER"]);
            
        $this->manager->persist($user);
    }
}
