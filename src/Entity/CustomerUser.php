<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CustomerUserRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CustomerUserRepository::class)]
/**
 * @ApiResource(
 *      denormalizationContext={"groups"={"customerUser:write"}},
 *      collectionOperations = {
 *          "post"
 *      },
 *      itemOperations = {
 *          "get" = {
 *              "normalization_context" = {"groups"={"customerUser:read"}}
 *          },
 *          "delete"
 *      }
 * )
 */
class CustomerUser
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    /**
     * @Groups({"customerUser:read","customerUser:user"})
     */
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    /**
     * @Groups({"customerUser:read","customerUser:write","customerUser:user"})
     * @Assert\NotBlank(message="Le pseudo est obligatoire")
     * @Assert\Type(type="string",message="Vous devez saisir une chaine de caracteres.")
     */
    private $username;

    #[ORM\Column(type: 'string', length: 255)]
    /**
     * @Groups({"customerUser:read","customerUser:write","customerUser:user"})
     * @Assert\NotBlank(message="Le prenom est obligatoire")
     * @Assert\Type(type="string",message="Vous devez saisir une chaine de caracteres.")
     */
    private $firstName;

    #[ORM\Column(type: 'string', length: 255)]
    /**
     * @Groups({"customerUser:read","customerUser:write","customerUser:user"})
     * @Assert\NotBlank(message="Le nom est obligatoire")
     * @Assert\Type(type="string",message="Vous devez saisir une chaine de caracteres.")
     */
    private $lastName;

    #[ORM\Column(type: 'string', length: 255)]
    /**
     * @Groups({"customerUser:read","customerUser:write","customerUser:user"})
     * @Assert\NotBlank(message="L'email est obligatoire")
     * @Assert\Email(message="Vous devez rentrer un email valide.")
     */
    private $email;

    #[ORM\Column(type: 'datetime')]
    /**
     * @Groups({"customerUser:read","customerUser:user"})
     */
    private $createdAt;

    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'customerUsers')]
    #[ORM\JoinColumn(nullable: false)]
    /**
     * @Groups({"customerUser:read","customerUser:write"})
     */
    private $customer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }
}
