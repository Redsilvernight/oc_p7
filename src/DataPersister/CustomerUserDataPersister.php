<?php
namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\CustomerUser;
use Doctrine\ORM\EntityManagerInterface;

class CustomerUserDataPersister implements DataPersisterInterface
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function supports($data): bool
    {
        return $data instanceof CustomerUser;
    }

    /**
    * @param CustomerUser data
    *
    * @return void
    */
    public function persist($data)
    {
        if (!$data->getId()) {
            $data->setCreatedAt(new \DateTime('now'));
        }

        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }

    public function remove($data)
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}