<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Controller\CustomerController;
use App\Repository\CustomerRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CustomerRepository::class)]
/**
 * @ApiResource(
 *      subresourceOperations={
 *          "api_questions_answer_get_subresource" = {
 *              "normalization_context" = {"groups" = {"customerUser:user"}}
 *          }
 *      },
 *      normalizationContext={"groups"= {"customerUser:user"}},
 *      collectionOperations = {
 *          
 *      },
 *      itemOperations = {
 *          "get" = {
 *              "openapi_context"={
 *                  "summary"="hidden"
 *              }
 *          }
 *      }
 * )
 */
class Customer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    /**
     * @Groups("customerUser:read")
     */
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    /**
     * @Groups("customerUser:read")
     */
    private $name;

    #[ORM\Column(type: 'text')]
    /**
     * @Groups("customerUser:read")
     */
    private $description;

    #[ORM\Column(type: 'string', length: 255)]
    /**
     * @Groups("customerUser:read")
     */
    private $email;

    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: CustomerUser::class, orphanRemoval: true)]
    /**
     * @ApiSubresource
     */
    private $customerUsers;

    public function __construct()
    {
        $this->customerUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection<int, CustomerUser>
     */
    public function getCustomerUsers(): Collection
    {
        return $this->customerUsers;
    }

    public function addCustomerUser(CustomerUser $customerUser): self
    {
        if (!$this->customerUsers->contains($customerUser)) {
            $this->customerUsers[] = $customerUser;
            $customerUser->setCustomer($this);
        }

        return $this;
    }

    public function removeCustomerUser(CustomerUser $customerUser): self
    {
        if ($this->customerUsers->removeElement($customerUser)) {
            // set the owning side to null (unless already changed)
            if ($customerUser->getCustomer() === $this) {
                $customerUser->setCustomer(null);
            }
        }

        return $this;
    }
}
