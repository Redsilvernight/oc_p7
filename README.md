# BilemoApi

## Generals Infos
Projet n°7 de la formation open classroom PHP/Symfony. Une api pour un vendeur de smartphone.

## Technologies
***
This project was made with :
* [PHP]: Version 8.0.10
* [Symfony]: Version 6.1
* [Apiplatform]: Version 2.6
* [jwt-authentication-bundle]: Version 2.15


## installation
***
Install this project with git and composer
***
* Clone repository
```
cd existing_repo
git remote add origin https://gitlab.com/Redsilvernight/oc_p7.git
git branch -M main
git push -uf origin main
```
* Update composer
```
$ composer update
```
* Generate jwt key
```
$ mkdir -p config/jwt
$ openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
$ openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```
* .env configuration
```
$ write your database configuration settings in DATABASE_URL variable
```
* create database
```
$ symfony console doctrine:database:create
```
* Install database
```
$ symfony console doctrine:migration:migrate
```
* Load Fixtures
```
$ symfony console doctrine:fixtures:loads
```
* Start symfony server
```
$ symfony server:start -d
```
* Read API Docs
```
$ /api/docs
