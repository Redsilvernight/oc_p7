<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ProductRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
/**
 * @ApiResource(
 *      normalizationContext={"groups"={"product:read"}},
 *      denormalizationContext={"groups"={"product:write"}},
 *      collectionOperations = {
 *          "get"
 *      },
 *      itemOperations = {
 *          "get",
 *          "put"
 *      }
 * )
 */
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    /**
     * @Groups({"product:read","product:write"})
     */
    private $name;

    #[ORM\Column(type: 'text')]
    /**
     * @Groups({"product:read","product:write"})
     */
    private $description;

    #[ORM\Column(type: 'string', length: 255)]
    /**
     * @Groups({"product:read","product:write"})
     */
    private $price;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }
}
